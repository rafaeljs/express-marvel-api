var express = require('express');
var router = express.Router();
var request = require('request');
var Marvel;
var perso;

var apikey = 'bd4261d8539b6c2dc460782d4636a9ac';
var hash = '979d43a37357930041f2eebbeeb408c2';




/* GET home page. */
router.get('/', function(req, res, next) {
    request('http://gateway.marvel.com/v1/public/events/29/characters?limit=50&ts=1&apikey='+apikey+'&hash='+hash, function (error, response, body) {
        Marvel = JSON.parse(body);
        res.render('index', { title: 'Personagens da Marvel' , marvel: Marvel});
    });
});

router.get('/personagem/:num',function (req, res, next) {
    request('http://gateway.marvel.com/v1/public/characters/'+req.params.num+'?limit=50&ts=1&apikey='+apikey+'&hash='+hash, function (error, response, body) {
        perso = JSON.parse(body);
        res.render('Personagem', {marvel: perso});
    });
});


module.exports = router;
